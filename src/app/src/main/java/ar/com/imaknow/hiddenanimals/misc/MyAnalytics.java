package ar.com.imaknow.hiddenanimals.misc;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by Acer VN7 on 22/11/2015.
 */
public class MyAnalytics {
    GoogleAnalytics analytics;
    Tracker tracker;

    public MyAnalytics(Context context) {
        analytics = GoogleAnalytics.getInstance(context);
        tracker = analytics.newTracker("UA-XXXX-Y"); // Send hits to tracker id UA-XXXX-Y
    }
}
