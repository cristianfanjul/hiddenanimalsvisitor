package ar.com.imaknow.hiddenanimals.ui.game;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import ar.com.imaknow.hiddenanimals.misc.ApplicationData;
import ar.com.imaknow.hiddenanimals.misc.Constants;

/**
 * Created by Acer VN7 on 11/08/2015.
 */
public class EndGamePresenter implements IEndGamePresenter {
    protected IEndGameView view;
    protected GameActivity activity;

    public EndGamePresenter(IEndGameView view, GameActivity activity) {
        this.view = view;
        this.activity = activity;
    }

    @Nullable
    @Override
    public void evaluateMaxScore(Bundle bundle) {
        if (bundle != null) {
            int score = bundle.getInt(ApplicationData.CURRENT_SCORE);
            ApplicationData appData = new ApplicationData(activity);
            if (score > appData.getMaxScore()) {
                appData.setMaxScore(score);
            }
        }
    }

    @Override
    public void openMainMenuScreen() {
        this.activity.finish();
    }

    @Override
    public void restartGame(String currentLevel) {
        Intent i = new Intent(activity, GameActivity.class);
        i.putExtra(Constants.LEVEL_SELECTED, currentLevel);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(i);
    }

    @Override
    public void startNextLevel(String currentLevel, Bundle bundle) {
        if (currentLevel.equals(Constants.LEVEL_1)) {
            bundle.putString(Constants.LEVEL_SELECTED, Constants.LEVEL_2);
        } else if (currentLevel.equals(Constants.LEVEL_2)) {
            bundle.putString(Constants.LEVEL_SELECTED, Constants.LEVEL_3);
        } else if (currentLevel.equals(Constants.LEVEL_3)) {
            bundle.putString(Constants.LEVEL_SELECTED, Constants.LEVEL_GO_FOR_ALL);
        }

        Intent i = new Intent(activity, GameActivity.class);
        i.putExtras(bundle);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(i);
    }
}
