package ar.com.imaknow.hiddenanimals.misc;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ar.com.imaknow.hiddenanimals.R;

/**
 * @author cfanjul
 */
public class MyCustomDialog extends Dialog {

    private RelativeLayout layoutTitleContainer;
    private TextView txtVwTitle;
    private ImageView imgVwTitle;
    private TextView txtVwMessage;
    private GameButton btnPositive;
    private GameButton btnNegative;
    private RelativeLayout relativeCoreContainer;
    private LinearLayout layoutButtonsContainer;

    public MyCustomDialog(Context context) {
        super(context);
        configureDialog();
    }

    private void configureDialog() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCanceledOnTouchOutside(false);

        // custom dialog
        setContentView(R.layout.custom_dialog);

        layoutTitleContainer = (RelativeLayout) findViewById(R.id.dialogTitleContainer);
        txtVwTitle = (TextView) findViewById(R.id.dialogTxtVwTitle);
        imgVwTitle = (ImageView) findViewById(R.id.dialogImgVwTitle);
        txtVwMessage = (TextView) findViewById(R.id.dialogTxtVwMessage);
        btnPositive = (GameButton) findViewById(R.id.dialogButtonOk);
        btnNegative = (GameButton) findViewById(R.id.dialogButtonCancel);
        relativeCoreContainer = (RelativeLayout) findViewById(R.id.dialogCoreContainer);
        layoutButtonsContainer = (LinearLayout) findViewById(R.id.dialogButtonContainer);

        addTypefaces();

        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public void hideButtons() {
        layoutButtonsContainer.setVisibility(View.GONE);
    }

    public void showButtons() {
        layoutButtonsContainer.setVisibility(View.VISIBLE);
    }

    public void enlargeOkButton() {
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT);
        btnPositive.setLayoutParams(params);
    }

    public void setTitle(String text) {
        txtVwTitle.setText(text);
    }

    public void setMessage(String text) {
        txtVwMessage.setText(text);
    }

    public void setPositiveButtonText(String text) {
        btnPositive.setText(text);
    }

    public void setNegativeButtonText(String text) {
        btnNegative.setText(text);
    }

    public void setPositiveButtonListener(View.OnClickListener listener) {
        btnPositive.setOnClickListener(listener);
    }

    public void setNegativeButtonListener(View.OnClickListener listener) {
        btnNegative.setOnClickListener(listener);
    }

    public View getPositiveButtonView() {
        return btnPositive;
    }

    public View getNegativeButtonView() {
        return btnNegative;
    }

    public LinearLayout getButtonsContainerLayout() {
        return layoutButtonsContainer;
    }

    public void setIcon(int drawable) {
//        imgVwTitle.setImageDrawable(AppViajantesApplication.getContext().getResources().getDrawable(drawable));
        imgVwTitle.setVisibility(View.VISIBLE);
    }

    public RelativeLayout getCoreContainer() {
        return relativeCoreContainer;
    }

    public void addTypefaces() {
        Typeface tp = Typeface.createFromAsset(getContext().getAssets(), ApplicationData.ANDROID_FONT);
        txtVwTitle.setTypeface(tp);
        txtVwMessage.setTypeface(tp);
        btnPositive.setTypeface(tp);
        btnNegative.setTypeface(tp);
    }
}
