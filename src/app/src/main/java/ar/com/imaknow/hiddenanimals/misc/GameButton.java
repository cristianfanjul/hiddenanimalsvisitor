package ar.com.imaknow.hiddenanimals.misc;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import ar.com.imaknow.hiddenanimals.R;

/**
 * Created by Acer VN7 on 10/08/2015.
 */
public class GameButton extends Button {
    private int extraPadding;
    private Rect rect;
    private int padding;

    public GameButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyButtonColor();
        setEventActions();
        setPaddingValue();
    }

    public GameButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setEventActions();
        setPaddingValue();
    }

    public GameButton(Context context) {
        super(context);
        setEventActions();
        setPaddingValue();
    }

    /**
     * Android 2.3 hack.
     */
    private void applyButtonColor() {
        setTextColor(Color.WHITE);
    }

    private void setPaddingValue() {
        padding = getResources().getDimensionPixelOffset(R.dimen.button_shadow_margin);
    }

    private void setEventActions() {
        extraPadding = getResources().getDimensionPixelOffset(R.dimen.motion_event_extra_padding);
        setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        rect = new Rect(v.getLeft() - extraPadding, v.getTop() - extraPadding, v.getRight() + extraPadding, v.getBottom() + extraPadding);
                        setPadding(0, padding, 0, 0);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (!rect.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())) {
                            setPadding(0, 0, 0, 0);
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        setPadding(0, 0, 0, 0);
                        break;
                }
                return false;
            }
        });
    }
}
