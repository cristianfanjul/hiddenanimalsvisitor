package ar.com.imaknow.hiddenanimals.models;

/**
 * Created by Acer VN7 on 14/07/2015.
 */
public class Answer {
    private String answer;
    private boolean isCorrect;

    public final String getAnswer() {
        return answer;
    }

    public final void setAnswer(String answer) {
        this.answer = answer;
    }

    public final boolean isCorrect() {
        return isCorrect;
    }

    public final void setIsCorrect(boolean isCorrect) {
        this.isCorrect = isCorrect;
    }
}
