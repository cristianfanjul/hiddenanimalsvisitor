package ar.com.imaknow.hiddenanimals.ui.game;

import android.os.Bundle;

/**
 * Created by Acer VN7 on 01/08/2015.
 */
public interface IEndGameView extends IBaseView {
    void setScore(Bundle bundle);

    int getScore();
}
