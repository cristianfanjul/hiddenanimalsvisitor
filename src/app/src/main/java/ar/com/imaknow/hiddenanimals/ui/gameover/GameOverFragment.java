package ar.com.imaknow.hiddenanimals.ui.gameover;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ar.com.imaknow.hiddenanimals.HiddenAnimalsApp;
import ar.com.imaknow.hiddenanimals.R;
import ar.com.imaknow.hiddenanimals.misc.Constants;
import ar.com.imaknow.hiddenanimals.misc.Utils;
import ar.com.imaknow.hiddenanimals.ui.game.EndGameFragment;

/**
 * Created by Acer VN7 on 01/08/2015.
 */
public class GameOverFragment extends EndGameFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HiddenAnimalsApp.getTracker().setScreenName(getString(R.string.ga_game_over));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        // Show click head tip for level 1.
        if (bundle != null && getCurrentLevel().equals(Constants.LEVEL_1)) {
            Utils.showTapHeadTip(getActivity());
        }

        return rootView;
    }

    @Override
    protected int getLayoutForFragment() {
        return R.layout.fragment_gameover;
    }
}
