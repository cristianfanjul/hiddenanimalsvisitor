package ar.com.imaknow.hiddenanimals;

import android.app.Application;
import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Acer VN7 on 19/07/2015.
 */
public class HiddenAnimalsApp extends Application {
    // The dispatching period in seconds when Google Play services is
    // unavailable. The default period is 1800 seconds or 30 minutes.
    private static final int DISPATCH_PERIOD = 1800;
    public static boolean IS_DEVELOPMENT = false;
    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

        TwitterAuthConfig authConfig = new TwitterAuthConfig(
                getString(R.string.twitter_consumer_key), getString(R.string.twitter_consumer_secret));
        Fabric.with(this, new Twitter(authConfig));

        initGoogleAnalytics();
    }

    private void initGoogleAnalytics() {
        analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(DISPATCH_PERIOD);

        tracker = analytics.newTracker(getString(R.string.google_analytics_tracker_id)); // Replace with actual tracker/property Id
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);
    }

    public static Context getContext() {
        return context;
    }

    public static Tracker getTracker() {
        return tracker;
    }
}
