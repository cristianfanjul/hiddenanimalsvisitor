package ar.com.imaknow.hiddenanimals.misc;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import ar.com.imaknow.hiddenanimals.R;

/**
 * Created by Acer VN7 on 25/07/2015.
 */
public class Utils {
    public static float calculatePorcentage(float measurePartial, float measureTotal) {
        return measurePartial * 100 / measureTotal;
    }

    public static int calculatePorcentage(int measurePartial, int measureTotal) {
        return measurePartial * 100 / measureTotal;
    }

    public static void showTapHeadTip(Context context) {
        Toast.makeText(context, R.string.tip_tap_head, Toast.LENGTH_LONG).show();
    }

    /*
    * getting screen width
    */
    public static int getScreenWidth(Context context) {
        int columnWidth;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point point = new Point();
        try {
            if (Build.VERSION.SDK_INT >= 13) {
                display.getSize(point);
            } else {
                point.x = display.getWidth();
                point.y = display.getHeight();
            }
        } catch (java.lang.NoSuchMethodError ignore) { // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }

        columnWidth = point.x;
        return columnWidth;
    }
}
