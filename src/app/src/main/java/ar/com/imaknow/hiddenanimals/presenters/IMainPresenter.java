package ar.com.imaknow.hiddenanimals.presenters;

import android.os.Bundle;

/**
 * Created by Acer VN7 on 12/07/2015.
 */
public interface IMainPresenter {
    void startGameActivity(Bundle bundle);
}
