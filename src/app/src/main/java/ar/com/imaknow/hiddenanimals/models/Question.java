package ar.com.imaknow.hiddenanimals.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acer VN7 on 14/07/2015.
 */
public class Question {
    private String question;
    private List<Answer> answersList = new ArrayList<>();

    public final String getQuestion() {
        return question;
    }

    public final void setQuestion(String question) {
        this.question = question;
    }

    public final List<Answer> getAnswersList() {
        return answersList;
    }

    public final void setAnswersList(List<Answer> answersList) {
        this.answersList = answersList;
    }
}
