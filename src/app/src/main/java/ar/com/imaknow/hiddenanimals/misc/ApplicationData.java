package ar.com.imaknow.hiddenanimals.misc;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Acer VN7 on 01/08/2015.
 */
public class ApplicationData {

    private static final String PREFERENCES_FILE_NAME = "HiddenAnimals";
    public static final String ANDROID_FONT = "fonts/android_7.ttf";

    private final int PRIVACY_MODE = Activity.MODE_PRIVATE;
    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor prefsEditor;

    private static final String MAX_SCORE = "MaxScore";
    public static final String CURRENT_SCORE = "CurrentScore";

    public ApplicationData(Context context) {
        appSharedPrefs = context.getSharedPreferences(PREFERENCES_FILE_NAME, PRIVACY_MODE);
        prefsEditor = appSharedPrefs.edit();
    }

    public void setMaxScore(int score) {
        prefsEditor.putInt(MAX_SCORE, score).apply();
    }

    public int getMaxScore() {
        return appSharedPrefs.getInt(MAX_SCORE, 0);
    }

    public void setLevel1Completed(boolean value) {
        prefsEditor.putBoolean(Constants.LEVEL_1, value).apply();
    }

    public boolean isLevel1Completed() {
        return appSharedPrefs.getBoolean(Constants.LEVEL_1, false);
    }

    public void setLevel2Completed(boolean value) {
        prefsEditor.putBoolean(Constants.LEVEL_2, value).apply();
    }

    public boolean isLevel2Completed() {
        return appSharedPrefs.getBoolean(Constants.LEVEL_2, false);
    }

    public void setLevel3Completed(boolean value) {
        prefsEditor.putBoolean(Constants.LEVEL_3, value).apply();
    }

    public boolean isLevel3Completed() {
        return appSharedPrefs.getBoolean(Constants.LEVEL_3, false);
    }

    public void setSoundEnabled(boolean value) {
        prefsEditor.putBoolean(Constants.SOUND_ENABLED, value).commit();
    }

    public boolean isSoundEnabled() {
        return appSharedPrefs.getBoolean(Constants.SOUND_ENABLED, true);
    }
}
