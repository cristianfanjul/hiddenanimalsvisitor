package ar.com.imaknow.hiddenanimals.models;

import java.util.ArrayList;
import java.util.List;

import ar.com.imaknow.hiddenanimals.HiddenAnimalsApp;
import ar.com.imaknow.hiddenanimals.R;

/**
 * Created by Acer VN7 on 13/07/2015.
 */
public class MyImage {
    private int resource;
    private int x;
    private int y;
    private int plusX;
    private int plusY;
    private Difficulty difficulty;
    private List<Question> questionList = new ArrayList<>();
    private String imageName;

    public MyImage(int resource, Difficulty difficulty, int x, int y, int plusX, int plusY, String imageName) {
        this.resource = resource;
        this.difficulty = difficulty;
        this.x = x;
        this.y = y;
        this.plusX = plusX;
        this.plusY = plusY;
        this.imageName = imageName;
    }

    public int getResource() {
        return resource;
    }

    public final void setResource(int resource) {
        this.resource = resource;
    }

    public final int getX() {
        return x;
    }

    public final void setX(int x) {
        this.x = x;
    }

    public final int getY() {
        return y;
    }

    public final void setY(int y) {
        this.y = y;
    }

    public final int getPlusX() {
        return plusX;
    }

    public final void setPlusX(int plusX) {
        this.plusX = plusX;
    }

    public final int getPlusY() {
        return plusY;
    }

    public final void setPlusY(int plusY) {
        this.plusY = plusY;
    }

    public final Difficulty getDifficulty() {
        return difficulty;
    }

    public final void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public String getImageName() {
        return imageName;
    }

    public List<Question> getQuestionList() {
        return questionList;
    }

    /**
     * Chechs if tapped X is in correct range comparing with porcentage in image.
     *
     * @param x
     * @return
     */
    public final boolean isRangeX(float x) {
        int enlargeTapArea = HiddenAnimalsApp.getContext().getResources().getInteger(R.integer.enlarge_tap_area);
        return (x > getX() - getPlusX() * enlargeTapArea) && (x < getX() + getPlusX() * enlargeTapArea);
    }

    /**
     * Chechs if tapped Y is in correct range comparing with porcentage in image.
     *
     * @param y
     * @return
     */
    public final boolean isRangeY(float y) {
        return (y > getY() - getPlusY()) && (y < getY() + getPlusY());
    }

    /**
     * Checks if parameters X and Y taped are in the range we want.
     *
     * @param x
     * @param y
     * @return
     */
    public final boolean isCorrectRange(float x, float y) {
        return isRangeX(x) && isRangeY(y);
    }
}
