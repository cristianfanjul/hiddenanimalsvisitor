package ar.com.imaknow.hiddenanimals.ui.gallery;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import ar.com.imaknow.hiddenanimals.R;
import ar.com.imaknow.hiddenanimals.models.ImageList;

/**
 * A placeholder fragment containing a simple view.
 */
public class GalleryFragment extends Fragment {

    private RecyclerViewImageAdapter mAdapter;
    private RecyclerView mRecyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mAdapter = new RecyclerViewImageAdapter(getActivity(), ImageList.getInstance().getMyImageList());
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gallery, container, false);

        initViews(rootView);

        return rootView;
    }

    private void initViews(View rootView) {
        initGridView(rootView);
        loadAdBanner(rootView);
    }

    private void initGridView(View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_vw_gallery);
        mRecyclerView.addItemDecoration(new MarginDecoration(getActivity()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        mRecyclerView.setAdapter(mAdapter);
    }

    private void loadAdBanner(View rootView) {
        // Load an ad into the AdMob banner view.
        AdView adView = (AdView) rootView.findViewById(R.id.ad_view);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("3657EA52DCFEA50DAF3D3C54C68BEE70").build();

        adView.loadAd(adRequest);
    }

    public class MarginDecoration extends RecyclerView.ItemDecoration {
        private int margin, marginTop;

        public MarginDecoration(Context context) {
            margin = context.getResources().getDimensionPixelSize(R.dimen.item_margin_sides);
        }

        @Override
        public void getItemOffsets(
                Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.set(margin, margin, margin, margin);
        }
    }
}
