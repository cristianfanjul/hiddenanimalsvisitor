package ar.com.imaknow.hiddenanimals.ui.main;

import android.view.View;

import ar.com.imaknow.hiddenanimals.navigators.GameNavigator;
import ar.com.imaknow.hiddenanimals.ui.game.IBaseView;

/**
 * Created by Acer VN7 on 12/07/2015.
 */
public interface IMainView extends IBaseView {
    GameNavigator getNavigator();

    void updateScore();

    void initializeViews(View rootView);
}
