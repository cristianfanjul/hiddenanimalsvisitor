package ar.com.imaknow.hiddenanimals.ui.main;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import ar.com.imaknow.hiddenanimals.R;
import ar.com.imaknow.hiddenanimals.misc.ApplicationData;
import ar.com.imaknow.hiddenanimals.misc.Constants;
import ar.com.imaknow.hiddenanimals.misc.GameButton;
import ar.com.imaknow.hiddenanimals.navigators.GameNavigator;
import ar.com.imaknow.hiddenanimals.presenters.MainPresenter;
import ar.com.imaknow.hiddenanimals.ui.BaseActivity;
import ar.com.imaknow.hiddenanimals.ui.gallery.GalleryActivity;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements IMainView {
    private MainPresenter mainPresenter;
    private TextView txtVwMaxScoreTitle;
    private TextView txtVwMaxScoreValue;
    private GameButton btnLevel1;
    private GameButton btnLevel2;
    private GameButton btnLevel3;
    private GameButton btnGoForAll;
    private Bundle bundle;
    private ApplicationData appData;
    private FloatingActionButton floattingBtn;
    private boolean isClickEnabled;
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main, container, false);

        appData = new ApplicationData(getActivity());
        bundle = new Bundle();
        mainPresenter = new MainPresenter(this);
        initializeViews(rootView);
        addTypefaces();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity) getActivity()).initSoundButton(rootView);
        updateScore();
        updateLevelsAvailables();
        enableClicks();
    }

    private void enableClicks() {
        isClickEnabled = true;
    }

    private void disableClicks() {
        isClickEnabled = false;
    }

    /**
     * Refresh buttons available to play a new level.
     */
    private void updateLevelsAvailables() {
        if (appData.isLevel1Completed()) {
            btnLevel2.setEnabled(true);
        }
        if (appData.isLevel2Completed()) {
            btnLevel3.setEnabled(true);
        }
        if (appData.isLevel3Completed()) {
            btnGoForAll.setVisibility(View.VISIBLE);
            btnGoForAll.setEnabled(true);
        }
    }

    @Override
    public void initializeViews(View rootView) {
        initBtnLevel1(rootView);
        initBtnLevel2(rootView);
        initBtnLevel3(rootView);
        initBtnGoForAll(rootView);
        initFloatingBtn(rootView);

        txtVwMaxScoreTitle = (TextView) rootView.findViewById(R.id.txt_vw_top_score_title);
        txtVwMaxScoreValue = (TextView) rootView.findViewById(R.id.txt_vw_top_score_value);

        loadAdBanner(rootView);
    }

    private void initFloatingBtn(View rootView) {
        floattingBtn = (FloatingActionButton) rootView.findViewById(R.id.fab);
        floattingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appData.isLevel1Completed() && isClickEnabled) {
                    startGalleryActivity();
                    disableClicks();
                } else {
                    showEnableGalleryMsg();
                }
            }
        });
    }

    private void showEnableGalleryMsg() {
        Toast.makeText(getActivity(), R.string.enable_galley, Toast.LENGTH_LONG).show();
    }

    private void startGalleryActivity() {
        Intent i = new Intent(getActivity(), GalleryActivity.class);
        i.putExtras(bundle);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    private void initBtnLevel1(View rootView) {
        btnLevel1 = (GameButton) rootView.findViewById(R.id.main_btn_level1);
        btnLevel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle.putString(Constants.LEVEL_SELECTED, Constants.LEVEL_1);
                mainPresenter.startGameActivity(bundle);
                disableClicks();
            }
        });
    }

    private void initBtnLevel2(View rootView) {
        btnLevel2 = (GameButton) rootView.findViewById(R.id.main_btn_level2);
        btnLevel2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isClickEnabled) {
                    bundle.putString(Constants.LEVEL_SELECTED, Constants.LEVEL_2);
                    mainPresenter.startGameActivity(bundle);
                    disableClicks();
                }
            }
        });

    }

    private void initBtnLevel3(View rootView) {
        btnLevel3 = (GameButton) rootView.findViewById(R.id.main_btn_level3);
        btnLevel3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isClickEnabled) {
                    bundle.putString(Constants.LEVEL_SELECTED, Constants.LEVEL_3);
                    mainPresenter.startGameActivity(bundle);
                    disableClicks();
                }
            }
        });

    }

    private void initBtnGoForAll(View rootView) {
        btnGoForAll = (GameButton) rootView.findViewById(R.id.main_btn_go_for_all);
        btnGoForAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isClickEnabled) {
                    bundle.putString(Constants.LEVEL_SELECTED, Constants.LEVEL_GO_FOR_ALL);
                    mainPresenter.startGameActivity(bundle);
                    disableClicks();
                }
            }
        });
    }

    @Override
    public GameNavigator getNavigator() {
        return (GameNavigator) getActivity();
    }

    @Override
    public void updateScore() {
        txtVwMaxScoreValue.setText(String.valueOf(new ApplicationData(getActivity()).getMaxScore()));
    }

    @Override
    public void addTypefaces() {
        Typeface tp = Typeface.createFromAsset(getActivity().getAssets(), ApplicationData.ANDROID_FONT);
        btnLevel1.setTypeface(tp);
        btnLevel2.setTypeface(tp);
        btnLevel3.setTypeface(tp);
        btnGoForAll.setTypeface(tp);
        txtVwMaxScoreTitle.setTypeface(tp);
        txtVwMaxScoreValue.setTypeface(tp);
    }

    private void loadAdBanner(View rootView) {
        // Load an ad into the AdMob banner view.
        AdView adView = (AdView) rootView.findViewById(R.id.ad_view);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }
}