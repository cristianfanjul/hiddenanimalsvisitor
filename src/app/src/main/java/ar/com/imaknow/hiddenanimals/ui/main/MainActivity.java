package ar.com.imaknow.hiddenanimals.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import ar.com.imaknow.hiddenanimals.HiddenAnimalsApp;
import ar.com.imaknow.hiddenanimals.R;
import ar.com.imaknow.hiddenanimals.navigators.GameNavigator;
import ar.com.imaknow.hiddenanimals.ui.BaseActivity;
import ar.com.imaknow.hiddenanimals.ui.game.GameActivity;


public class MainActivity extends BaseActivity implements GameNavigator {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FacebookSdk.sdkInitialize(getApplicationContext());
        HiddenAnimalsApp.getTracker().setScreenName(getString(R.string.ga_main_activity_screen));
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void navigateToGame(Bundle bundle) {
        Intent i = new Intent(this, GameActivity.class);
        i.putExtras(bundle);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }
}
