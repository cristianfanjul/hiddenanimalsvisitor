package ar.com.imaknow.hiddenanimals.navigators;

import android.os.Bundle;

/**
 * Created by Acer VN7 on 13/07/2015.
 */
public interface GameNavigator {
    void navigateToGame(Bundle bundle);
}
