package ar.com.imaknow.hiddenanimals.ui.game;

import android.os.Bundle;

/**
 * Created by Acer VN7 on 11/08/2015.
 */
public interface IEndGamePresenter {
    /**
     * Checks if current score is higher than the last max score.
     *
     * @param bundle
     */
    void evaluateMaxScore(Bundle bundle);

    void openMainMenuScreen();

    void restartGame(String currentLevel);

    void startNextLevel(String currentLevel, Bundle bundle);
}
