package ar.com.imaknow.hiddenanimals.models;

/**
 * Created by Acer VN7 on 13/07/2015.
 */
public class DifficultyFactory {
    public static final int EASY_TIME = 10;
    public static final int MEDIUM_TIME = 25;
    public static final int HARD_TIME = 60;

    public static final int EASY_POINTS = 100;
    public static final int MEDIUM_POINTS = 200;
    public static final int HARD_POINTS = 300;

    private static Difficulty easyDifficulty = new Difficulty(DifficultType.EASY,
                                                              EASY_TIME,
                                                              EASY_POINTS);
    private static Difficulty mediumDifficulty = new Difficulty(DifficultType.MEDIUM,
                                                                MEDIUM_TIME,
                                                                MEDIUM_POINTS);
    private static Difficulty hardDifficulty = new Difficulty(DifficultType.HARD,
                                                              HARD_TIME,
                                                              HARD_POINTS);

    public static final Difficulty getDifficulltyInstance(DifficultType difficultType) {
        switch (difficultType.ordinal()) {
        case 0:
            return easyDifficulty;
        case 1:
            return mediumDifficulty;
        case 2:
            return hardDifficulty;
        default:
            return new Difficulty(DifficultType.EASY, EASY_TIME, EASY_POINTS);
        }
    }
}
