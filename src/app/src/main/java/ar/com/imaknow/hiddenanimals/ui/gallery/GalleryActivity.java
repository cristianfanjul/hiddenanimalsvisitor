package ar.com.imaknow.hiddenanimals.ui.gallery;

import android.os.Bundle;

import ar.com.imaknow.hiddenanimals.HiddenAnimalsApp;
import ar.com.imaknow.hiddenanimals.R;
import ar.com.imaknow.hiddenanimals.misc.ApplicationData;
import ar.com.imaknow.hiddenanimals.models.ImageList;
import ar.com.imaknow.hiddenanimals.ui.BaseActivity;

public class GalleryActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        generateImageList();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        HiddenAnimalsApp.getTracker().setScreenName(getString(R.string.ga_gallery_activity));
    }

    private void generateImageList() {
        ImageList.getInstance().clear();
        ApplicationData appData = new ApplicationData(this);
        if (appData.isLevel1Completed()) {
            ImageList.getInstance().createLevelOneImageList();
        }

        if (appData.isLevel2Completed()) {
            ImageList.getInstance().createLevelOneImageList();
        }

        if (appData.isLevel3Completed()) {
            ImageList.getInstance().createLevelOneImageList();
        }
    }

}
