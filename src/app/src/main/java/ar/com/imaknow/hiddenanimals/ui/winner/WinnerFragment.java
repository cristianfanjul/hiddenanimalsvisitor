package ar.com.imaknow.hiddenanimals.ui.winner;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ar.com.imaknow.hiddenanimals.HiddenAnimalsApp;
import ar.com.imaknow.hiddenanimals.R;
import ar.com.imaknow.hiddenanimals.ui.game.EndGameFragment;

/**
 * Created by Acer VN7 on 01/08/2015.
 */
public class WinnerFragment extends EndGameFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HiddenAnimalsApp.getTracker().setScreenName(getString(R.string.ga_win_game));
    }

    @Override
    protected int getLayoutForFragment() {
        return R.layout.fragment_winner;
    }
}
