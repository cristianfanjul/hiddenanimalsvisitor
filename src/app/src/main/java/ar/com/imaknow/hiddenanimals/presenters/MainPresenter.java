package ar.com.imaknow.hiddenanimals.presenters;

import android.os.Bundle;

import ar.com.imaknow.hiddenanimals.ui.main.IMainView;

/**
 * Created by Acer VN7 on 12/07/2015.
 */
public class MainPresenter implements IMainPresenter {
    private IMainView view;

    public MainPresenter(IMainView iMainView) {
        this.view = iMainView;
    }

    @Override
    public void startGameActivity(Bundle bundle) {
        this.view.getNavigator().navigateToGame(bundle);
    }
}
