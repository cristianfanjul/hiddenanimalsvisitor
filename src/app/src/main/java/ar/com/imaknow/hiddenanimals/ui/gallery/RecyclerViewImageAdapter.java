package ar.com.imaknow.hiddenanimals.ui.gallery;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ar.com.imaknow.hiddenanimals.R;
import ar.com.imaknow.hiddenanimals.misc.Utils;
import ar.com.imaknow.hiddenanimals.models.MyImage;

public class RecyclerViewImageAdapter extends RecyclerView.Adapter<RecyclerViewImageAdapter.CustomViewHolder> {
    public static final int NUM_OF_COLUMNS = 3;
    public static final int GRID_PADDING = 0; // in dp

    private Activity mActivity;
    private List<MyImage> mMyImageList = new ArrayList();

    public RecyclerViewImageAdapter(Activity activity, List<MyImage> myImageList) {
        mActivity = activity;
        mMyImageList = myImageList;
    }

    @Override
    public RecyclerViewImageAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.partial_gallery_item, null);

        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int position) {
        MyImage image = mMyImageList.get(position);

        Glide.with(mActivity)
                .load(image.getResource())
                .centerCrop()
                .crossFade()
                .into(customViewHolder.imageView);

        customViewHolder.imageView.setOnClickListener(new OnImageClickListener(position));
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return (null != mMyImageList ? mMyImageList.size() : 0);
    }

    class OnImageClickListener implements OnClickListener {
        int postion;

        public OnImageClickListener(int position) {
            this.postion = position;
        }

        @Override
        public void onClick(View v) {
            Intent i = new Intent(mActivity, FullScreenViewActivity.class);
            i.putExtra("position", postion);
            mActivity.startActivity(i);
        }
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imageView;

        public CustomViewHolder(View view) {
            super(view);
            Resources r = view.getContext().getResources();
            float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, GRID_PADDING, r.getDisplayMetrics());
            int columnWidth = (int) ((Utils.getScreenWidth(view.getContext()) - ((NUM_OF_COLUMNS + 1) * padding)) / NUM_OF_COLUMNS);

            imageView = (ImageView) view.findViewById(R.id.gallery_img_vw_item);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(columnWidth, columnWidth);
            imageView.setLayoutParams(params);
        }
    }
}

