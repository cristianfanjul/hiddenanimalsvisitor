package ar.com.imaknow.hiddenanimals.models;

/**
 * Created by Acer VN7 on 13/07/2015.
 */
public enum DifficultType {
    EASY("easy"),
    MEDIUM("medium"),
    HARD("hard");

    private final String text;

    DifficultType(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
