package ar.com.imaknow.hiddenanimals.presenters;

import android.os.Bundle;

import ar.com.imaknow.hiddenanimals.models.MyImage;

/**
 * Created by Acer VN7 on 15/07/2015.
 */
public interface IGamePresenter {
    void startGame();

    MyImage getNextImage();

    MyImage getNextImage(int currentIndex);

    boolean isLastImage();

    boolean isLastImage(int currentIndex);

    boolean tapCorrectPoint(MyImage myImage, float tapedX, float tapedY);

    void sumPoints();

    void remarkRemovedLife();

    void removeLife();

    void checkLivesQty();

    void endGame();

    void animateLife();

    void addWrongTap();

    void addWrongFastTap();

    void clearTapCounter();

    void clearFastTapCounter();

    void checkMultipleWrongTaps();

    void fireFastTapsHandler();

    boolean isWrongTapLimit();

    boolean isFastTapLimit();

    void loadSounds();

    void playCorrectSound();

    void playLoseLifeSound();

    boolean isMoving(float initialX, float finalX, float initialY, float finalY);

    void updateCompletedLevel(Bundle bundle);

    void sendGALostLifeTimeEvent(String imageName);

    void sendGALostLifeTouchesEvent(String imageName);
}
