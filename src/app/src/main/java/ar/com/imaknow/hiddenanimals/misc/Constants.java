package ar.com.imaknow.hiddenanimals.misc;

/**
 * Created by Acer VN7 on 11/10/2015.
 */
public class Constants {
    public static final String LEVEL_SELECTED = "level_selected";
    public static final String LEVEL_1 = "level_1";
    public static final String LEVEL_2 = "level_2";
    public static final String LEVEL_3 = "level_3";
    public static final String LEVEL_GO_FOR_ALL = "go_for_all";
    public static final String WIN_LEVEL = "win_level";
    public static final String SOUND_ENABLED = "sound_enabled";
}
