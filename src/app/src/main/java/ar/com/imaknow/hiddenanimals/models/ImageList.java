package ar.com.imaknow.hiddenanimals.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ar.com.imaknow.hiddenanimals.R;

/**
 * Created by Acer VN7 on 14/07/2015.
 */
public class ImageList {
    public static final int MINIMUM_AREA = 5;
    public static final int MINIMUM_BIGGER_AREA = 8;
    public static final int BIG_AREA = 15;
    public static final int MEDIUM_AREA = 10;
    private static ImageList instance = new ImageList();
    private List<MyImage> myImageList = new ArrayList<>();
    Difficulty easy = DifficultyFactory.getDifficulltyInstance(DifficultType.EASY);
    Difficulty medium = DifficultyFactory.getDifficulltyInstance(DifficultType.MEDIUM);
    Difficulty hard = DifficultyFactory.getDifficulltyInstance(DifficultType.HARD);

    public static ImageList getInstance() {
        return instance;
    }

    public List<MyImage> getMyImageList() {
        return myImageList;
    }

    public void createLevelOneImageList() {
        myImageList.add(new MyImage(R.drawable.easy_1, easy, 49, 58, MINIMUM_AREA, MINIMUM_AREA, "e1"));
        myImageList.add(new MyImage(R.drawable.easy_2, easy, 31, 72, MINIMUM_AREA, MEDIUM_AREA, "e2"));
        myImageList.add(new MyImage(R.drawable.easy_3, easy, 80, 56, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e3"));
        myImageList.add(new MyImage(R.drawable.easy_4, easy, 33, 40, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e4"));
        myImageList.add(new MyImage(R.drawable.easy_5, easy, 18, 36, BIG_AREA, BIG_AREA, "e5"));
        myImageList.add(new MyImage(R.drawable.easy_6, easy, 53, 43, MINIMUM_AREA, MINIMUM_AREA, "e6"));

        myImageList.add(new MyImage(R.drawable.easy_7, easy, 23, 48, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e7"));
        myImageList.add(new MyImage(R.drawable.easy_8, easy, 28, 29, MINIMUM_AREA, MINIMUM_AREA, "e8"));
        myImageList.add(new MyImage(R.drawable.easy_9, easy, 48, 30, MINIMUM_AREA, MINIMUM_AREA, "e9"));
        myImageList.add(new MyImage(R.drawable.easy_10, easy, 66, 59, BIG_AREA, BIG_AREA, "e10"));
        myImageList.add(new MyImage(R.drawable.easy_11, easy, 64, 51, BIG_AREA, BIG_AREA, "e11"));
        myImageList.add(new MyImage(R.drawable.easy_12, easy, 56, 56, MINIMUM_AREA, MINIMUM_AREA, "e12"));

        myImageList.add(new MyImage(R.drawable.easy_13, easy, 52, 50, MINIMUM_AREA, MINIMUM_AREA, "e13"));
        myImageList.add(new MyImage(R.drawable.easy_14, easy, 68, 32, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e14"));
        myImageList.add(new MyImage(R.drawable.easy_16, easy, 32, 57, MINIMUM_AREA, MEDIUM_AREA, "e16"));

        myImageList.add(new MyImage(R.drawable.easy_19, easy, 35, 20, MINIMUM_AREA, MINIMUM_AREA, "e19"));
        myImageList.add(new MyImage(R.drawable.easy_20, easy, 34, 53, MINIMUM_AREA, MINIMUM_AREA, "e20"));
        myImageList.add(new MyImage(R.drawable.easy_21, easy, 13, 58, MINIMUM_AREA, MINIMUM_AREA, "e21"));
        myImageList.add(new MyImage(R.drawable.easy_22, easy, 72, 20, MINIMUM_AREA, MINIMUM_AREA, "e22"));
        myImageList.add(new MyImage(R.drawable.easy_23, easy, 44, 55, MINIMUM_AREA, MINIMUM_AREA, "e23"));
        myImageList.add(new MyImage(R.drawable.easy_24, easy, 59, 60, MINIMUM_AREA, MINIMUM_AREA, "e24"));

        myImageList.add(new MyImage(R.drawable.easy_25, easy, 33, 69, MINIMUM_AREA, MINIMUM_AREA, "e25"));
        myImageList.add(new MyImage(R.drawable.easy_26, easy, 45, 66, MINIMUM_AREA, MINIMUM_AREA, "e26"));
        myImageList.add(new MyImage(R.drawable.easy_28, easy, 45, 53, MEDIUM_AREA, MEDIUM_AREA, "e28"));
        myImageList.add(new MyImage(R.drawable.easy_29, easy, 43, 45, BIG_AREA, BIG_AREA, "e29"));
        myImageList.add(new MyImage(R.drawable.easy_30, easy, 46, 33, MINIMUM_AREA, MINIMUM_AREA, "e30"));

        myImageList.add(new MyImage(R.drawable.easy_31, easy, 48, 48, MEDIUM_AREA, MEDIUM_AREA, "e31"));
        myImageList.add(new MyImage(R.drawable.easy_32, easy, 50, 33, MEDIUM_AREA, MEDIUM_AREA, "e32"));
        myImageList.add(new MyImage(R.drawable.easy_33, easy, 25, 56, MINIMUM_AREA, MINIMUM_AREA, "e33"));
        myImageList.add(new MyImage(R.drawable.easy_34, easy, 55, 34, MINIMUM_AREA, MINIMUM_AREA, "e34"));
        myImageList.add(new MyImage(R.drawable.easy_36, easy, 34, 48, MINIMUM_AREA, MINIMUM_AREA, "e36"));

        myImageList.add(new MyImage(R.drawable.easy_37, easy, 33, 64, MINIMUM_AREA, MINIMUM_AREA, "e37"));
        myImageList.add(new MyImage(R.drawable.easy_38, easy, 57, 50, MINIMUM_AREA, MINIMUM_AREA, "e38"));
        myImageList.add(new MyImage(R.drawable.easy_39, easy, 63, 69, MEDIUM_AREA, MEDIUM_AREA, "e39"));
        myImageList.add(new MyImage(R.drawable.easy_40, easy, 50, 50, MINIMUM_AREA, MINIMUM_AREA, "e40"));
        myImageList.add(new MyImage(R.drawable.easy_41, easy, 51, 43, MINIMUM_AREA, MINIMUM_AREA, "e41"));

        myImageList.add(new MyImage(R.drawable.easy_44, easy, 53, 60, MINIMUM_AREA, MINIMUM_AREA, "e44"));
        myImageList.add(new MyImage(R.drawable.easy_45, easy, 26, 31, MINIMUM_AREA, MINIMUM_AREA, "e45"));
        myImageList.add(new MyImage(R.drawable.easy_46, easy, 50, 32, MINIMUM_AREA, MINIMUM_AREA, "e46"));
        myImageList.add(new MyImage(R.drawable.easy_47, easy, 22, 63, MINIMUM_AREA, MINIMUM_AREA, "e47"));
        myImageList.add(new MyImage(R.drawable.easy_48, easy, 40, 60, MINIMUM_AREA, MINIMUM_AREA, "e48"));

        myImageList.add(new MyImage(R.drawable.easy_50, easy, 55, 31, MEDIUM_AREA, MEDIUM_AREA, "e50"));
        myImageList.add(new MyImage(R.drawable.easy_51, easy, 33, 63, MEDIUM_AREA, MEDIUM_AREA, "e51"));
        myImageList.add(new MyImage(R.drawable.easy_52, easy, 66, 65, MEDIUM_AREA, MEDIUM_AREA, "e52"));
        myImageList.add(new MyImage(R.drawable.easy_53, easy, 41, 18, MEDIUM_AREA, MEDIUM_AREA, "e53"));
        myImageList.add(new MyImage(R.drawable.easy_54, easy, 41, 23, MEDIUM_AREA, MEDIUM_AREA, "e54"));

        myImageList.add(new MyImage(R.drawable.easy_55, easy, 69, 74, MINIMUM_AREA, MINIMUM_AREA, "e55"));
        myImageList.add(new MyImage(R.drawable.easy_56, easy, 30, 57, MEDIUM_AREA, MEDIUM_AREA, "e56"));
        myImageList.add(new MyImage(R.drawable.easy_57, easy, 62, 53, MEDIUM_AREA, MEDIUM_AREA, "e57"));
        myImageList.add(new MyImage(R.drawable.easy_58, easy, 52, 45, MEDIUM_AREA, MEDIUM_AREA, "e58"));
    }

    public void createLevelTwoImageList() {
        myImageList.add(new MyImage(R.drawable.easy_59, easy, 48, 24, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e59"));
        myImageList.add(new MyImage(R.drawable.easy_60, easy, 52, 18, MEDIUM_AREA, MEDIUM_AREA, "e60"));

        myImageList.add(new MyImage(R.drawable.easy_61, easy, 36, 34, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e61"));
        myImageList.add(new MyImage(R.drawable.easy_62, easy, 39, 50, MINIMUM_AREA, MINIMUM_AREA, "e62"));
        myImageList.add(new MyImage(R.drawable.easy_63, easy, 54, 80, MINIMUM_AREA, MINIMUM_AREA, "e63"));
        myImageList.add(new MyImage(R.drawable.easy_65, easy, 61, 26, MINIMUM_AREA, MINIMUM_AREA, "e65"));
        myImageList.add(new MyImage(R.drawable.easy_66, easy, 26, 51, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e66"));

        myImageList.add(new MyImage(R.drawable.easy_67, easy, 56, 66, BIG_AREA, BIG_AREA, "e67"));
        myImageList.add(new MyImage(R.drawable.easy_68, easy, 58, 32, MINIMUM_AREA, MINIMUM_AREA, "e68"));
        myImageList.add(new MyImage(R.drawable.easy_69, easy, 65, 24, MINIMUM_AREA, MINIMUM_AREA, "e69"));
        myImageList.add(new MyImage(R.drawable.easy_70, easy, 72, 25, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e70"));
        myImageList.add(new MyImage(R.drawable.easy_71, easy, 45, 49, MEDIUM_AREA, MEDIUM_AREA, "e71"));
        myImageList.add(new MyImage(R.drawable.easy_72, easy, 76, 55, MINIMUM_AREA, MINIMUM_AREA, "e72"));

        myImageList.add(new MyImage(R.drawable.easy_73, easy, 34, 27, MEDIUM_AREA, MEDIUM_AREA, "e73"));
        myImageList.add(new MyImage(R.drawable.easy_74, easy, 60, 36, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e74"));
        myImageList.add(new MyImage(R.drawable.easy_75, easy, 32, 50, MINIMUM_AREA, MINIMUM_AREA, "e75"));
        myImageList.add(new MyImage(R.drawable.easy_76, easy, 57, 33, MINIMUM_AREA, MINIMUM_AREA, "e76"));
        myImageList.add(new MyImage(R.drawable.easy_77, easy, 56, 45, MINIMUM_AREA, MINIMUM_AREA, "e77"));
        myImageList.add(new MyImage(R.drawable.easy_78, easy, 31, 24, MINIMUM_AREA, MINIMUM_AREA, "e78"));

        myImageList.add(new MyImage(R.drawable.easy_80, easy, 35, 27, MINIMUM_AREA, MINIMUM_AREA, "e80"));
        myImageList.add(new MyImage(R.drawable.easy_81, easy, 21, 57, MINIMUM_AREA, MINIMUM_AREA, "e81"));
        myImageList.add(new MyImage(R.drawable.easy_82, easy, 35, 30, MINIMUM_AREA, MINIMUM_AREA, "e82"));
        myImageList.add(new MyImage(R.drawable.easy_83, easy, 21, 39, MINIMUM_AREA, MINIMUM_AREA, "e83"));
        myImageList.add(new MyImage(R.drawable.easy_84, easy, 45, 29, MINIMUM_AREA, MINIMUM_AREA, "e84"));

        myImageList.add(new MyImage(R.drawable.easy_85, easy, 61, 49, MINIMUM_AREA, MINIMUM_AREA, "e85"));
        myImageList.add(new MyImage(R.drawable.easy_87, easy, 56, 44, MINIMUM_AREA, MINIMUM_AREA, "e87"));
        myImageList.add(new MyImage(R.drawable.easy_88, easy, 61, 76, MEDIUM_AREA, MEDIUM_AREA, "e88"));
        myImageList.add(new MyImage(R.drawable.easy_90, easy, 54, 46, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e90"));

        myImageList.add(new MyImage(R.drawable.easy_91, easy, 50, 42, MINIMUM_AREA, MINIMUM_AREA, "e91"));
        myImageList.add(new MyImage(R.drawable.easy_92, easy, 61, 38, MINIMUM_AREA, MINIMUM_AREA, "e92"));
        myImageList.add(new MyImage(R.drawable.easy_93, easy, 51, 33, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e93"));
        myImageList.add(new MyImage(R.drawable.easy_94, easy, 49, 24, MINIMUM_AREA, MINIMUM_AREA, "e94"));
        myImageList.add(new MyImage(R.drawable.easy_95, easy, 47, 32, MINIMUM_AREA, MINIMUM_AREA, "e95"));
        myImageList.add(new MyImage(R.drawable.easy_96, easy, 54, 36, MINIMUM_AREA, MINIMUM_AREA, "e96"));

        myImageList.add(new MyImage(R.drawable.easy_97, easy, 47, 40, MEDIUM_AREA, MEDIUM_AREA, "e97"));
        myImageList.add(new MyImage(R.drawable.easy_98, easy, 62, 76, MINIMUM_AREA, MINIMUM_AREA, "e98"));
        myImageList.add(new MyImage(R.drawable.easy_99, easy, 49, 30, MINIMUM_AREA, MINIMUM_AREA, "e99"));
        myImageList.add(new MyImage(R.drawable.easy_100, easy, 55, 19, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e100"));
        myImageList.add(new MyImage(R.drawable.easy_101, easy, 59, 38, MEDIUM_AREA, MEDIUM_AREA, "e101"));
        myImageList.add(new MyImage(R.drawable.easy_102, easy, 50, 47, MEDIUM_AREA, MEDIUM_AREA, "e102"));

        myImageList.add(new MyImage(R.drawable.easy_103, easy, 69, 22, MINIMUM_AREA, MINIMUM_AREA, "e103"));
        myImageList.add(new MyImage(R.drawable.easy_104, easy, 35, 47, MINIMUM_AREA, MINIMUM_AREA, "e104"));
        myImageList.add(new MyImage(R.drawable.easy_105, easy, 60, 50, MINIMUM_AREA, MINIMUM_AREA, "e105"));
        myImageList.add(new MyImage(R.drawable.easy_106, easy, 40, 19, MINIMUM_AREA, MINIMUM_AREA, "e106"));
        myImageList.add(new MyImage(R.drawable.easy_107, easy, 44, 50, MINIMUM_AREA, MINIMUM_AREA, "e107"));
        myImageList.add(new MyImage(R.drawable.easy_108, easy, 61, 31, MINIMUM_AREA, MINIMUM_AREA, "e108"));

        myImageList.add(new MyImage(R.drawable.easy_109, easy, 50, 46, MINIMUM_AREA, MINIMUM_AREA, "e109"));
        myImageList.add(new MyImage(R.drawable.easy_110, easy, 48, 48, MINIMUM_AREA, MINIMUM_AREA, "e110"));
        myImageList.add(new MyImage(R.drawable.easy_112, easy, 27, 50, MINIMUM_AREA, MINIMUM_AREA, "e112"));
        myImageList.add(new MyImage(R.drawable.easy_113, easy, 47, 48, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e113"));
    }

    public void createLevelThreeImageList() {
        myImageList.add(new MyImage(R.drawable.easy_114, easy, 66, 48, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e114"));
        myImageList.add(new MyImage(R.drawable.easy_115, easy, 33, 50, MEDIUM_AREA, MEDIUM_AREA, "e115"));
        myImageList.add(new MyImage(R.drawable.easy_116, easy, 71, 49, BIG_AREA, BIG_AREA, "e116"));
        myImageList.add(new MyImage(R.drawable.easy_117, easy, 54, 50, MEDIUM_AREA, MEDIUM_AREA, "e117"));
        myImageList.add(new MyImage(R.drawable.easy_118, easy, 51, 51, MEDIUM_AREA, MEDIUM_AREA, "e118"));
        myImageList.add(new MyImage(R.drawable.easy_119, easy, 51, 20, MEDIUM_AREA, MEDIUM_AREA, "e119"));
        myImageList.add(new MyImage(R.drawable.easy_120, easy, 49, 45, MINIMUM_AREA, MINIMUM_AREA, "e120"));

        myImageList.add(new MyImage(R.drawable.easy_121, easy, 59, 44, MEDIUM_AREA, MEDIUM_AREA, "e121"));
        myImageList.add(new MyImage(R.drawable.easy_122, easy, 60, 63, MINIMUM_AREA, MINIMUM_AREA, "e122"));
        myImageList.add(new MyImage(R.drawable.easy_123, easy, 62, 32, MEDIUM_AREA, MEDIUM_AREA, "e123"));
        myImageList.add(new MyImage(R.drawable.easy_124, easy, 58, 44, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e124"));
        myImageList.add(new MyImage(R.drawable.easy_125, easy, 51, 28, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e125"));
        myImageList.add(new MyImage(R.drawable.easy_126, easy, 59, 54, MINIMUM_AREA, MINIMUM_AREA, "e126"));
        myImageList.add(new MyImage(R.drawable.easy_127, easy, 62, 76, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "e127"));

        myImageList.add(new MyImage(R.drawable.medium_1, medium, 54, 38, MINIMUM_AREA, MINIMUM_AREA, "m1"));
        myImageList.add(new MyImage(R.drawable.medium_2, medium, 74, 52, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "m2"));
        myImageList.add(new MyImage(R.drawable.medium_3, medium, 20, 24, MINIMUM_AREA, MINIMUM_AREA, "m3"));
        myImageList.add(new MyImage(R.drawable.medium_5, medium, 16, 28, MINIMUM_AREA, MINIMUM_AREA, "m5"));
        myImageList.add(new MyImage(R.drawable.medium_6, medium, 65, 54, MINIMUM_AREA, MINIMUM_AREA, "m6"));

        myImageList.add(new MyImage(R.drawable.medium_7, medium, 40, 57, MEDIUM_AREA, MEDIUM_AREA, "m7"));
        myImageList.add(new MyImage(R.drawable.medium_8, medium, 35, 31, MINIMUM_AREA, MINIMUM_AREA, "m8"));
        myImageList.add(new MyImage(R.drawable.medium_9, medium, 66, 33, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "m9"));
        myImageList.add(new MyImage(R.drawable.medium_11, medium, 23, 13, MINIMUM_AREA, MINIMUM_AREA, "m11"));
        myImageList.add(new MyImage(R.drawable.medium_12, medium, 37, 43, MINIMUM_AREA, MINIMUM_AREA, "m12"));

        myImageList.add(new MyImage(R.drawable.medium_13, medium, 38, 47, MINIMUM_AREA, MINIMUM_AREA, "m13"));
        myImageList.add(new MyImage(R.drawable.medium_14, medium, 64, 54, MINIMUM_AREA, MINIMUM_AREA, "m14"));
        myImageList.add(new MyImage(R.drawable.medium_15, medium, 60, 53, MINIMUM_AREA, MINIMUM_AREA, "m15"));
        myImageList.add(new MyImage(R.drawable.medium_16, medium, 51, 32, MINIMUM_BIGGER_AREA, MINIMUM_BIGGER_AREA, "m16"));
        myImageList.add(new MyImage(R.drawable.medium_17, medium, 62, 27, MINIMUM_AREA, MINIMUM_AREA, "m17"));
        myImageList.add(new MyImage(R.drawable.medium_18, medium, 29, 50, MINIMUM_AREA, MINIMUM_AREA, "m18"));

        myImageList.add(new MyImage(R.drawable.medium_19, medium, 48, 44, MINIMUM_AREA, MINIMUM_AREA, "m19"));
        myImageList.add(new MyImage(R.drawable.medium_20, medium, 55, 66, MINIMUM_AREA, MINIMUM_AREA, "m20"));

        myImageList.add(new MyImage(R.drawable.hard_1, hard, 27, 65, MINIMUM_AREA, MINIMUM_AREA, "h1"));
        myImageList.add(new MyImage(R.drawable.hard_2, hard, 35, 77, MINIMUM_AREA, MINIMUM_AREA, "h2"));
        myImageList.add(new MyImage(R.drawable.hard_3, hard, 50, 48, MINIMUM_AREA, MINIMUM_AREA, "h3"));
        myImageList.add(new MyImage(R.drawable.hard_4, hard, 62, 43, MINIMUM_AREA, MINIMUM_AREA, "h4"));
        myImageList.add(new MyImage(R.drawable.hard_5, hard, 75, 42, MINIMUM_AREA, MINIMUM_AREA, "h5"));
        myImageList.add(new MyImage(R.drawable.hard_6, hard, 49, 39, MINIMUM_AREA, MINIMUM_AREA, "h6"));
        myImageList.add(new MyImage(R.drawable.hard_7, hard, 59, 54, MINIMUM_AREA, MINIMUM_AREA, "h7"));
    }

    public void createFullImageList() {
        createLevelOneImageList();
        createLevelTwoImageList();
        createLevelThreeImageList();
    }

    public MyImage getNextImage(int currentIndex) {
        MyImage myImage;
        if (currentIndex >= myImageList.size()) {
            myImage = ImageList.getInstance().getMyImageList().get(myImageList.size() - 1);
        } else {
            myImage = ImageList.getInstance().getMyImageList().get(currentIndex);
        }
        return myImage;
    }

    public boolean isLastImage(int currentIndex) {
        if (ImageList.getInstance().getMyImageList().size() == currentIndex) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isLastImage() {
        if (ImageList.getInstance().getMyImageList().size() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public MyImage getNextImage() {
        int i = getRandomIndexForList();
        MyImage myImage = ImageList.getInstance().getMyImageList().get(i);
        removeItem(i);
        return myImage;
    }

    public void removeItem(int index) {
        if (ImageList.getInstance().getMyImageList().size() > 1) {
            ImageList.getInstance().getMyImageList().remove(index);
        }
    }

    public int getRandomIndexForList() {
        int size = ImageList.getInstance().getMyImageList().size();
        Random generator = new Random();
        return size > 1 ? generator.nextInt(size - 1) : 0;
    }

    public void clear() {
        myImageList.clear();
    }
}
