package ar.com.imaknow.hiddenanimals.misc;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class SoundLoader {
    private static final String TAG = SoundLoader.class.getName();

    private AudioManager mAudioManager;
    private SoundPool soundPool;
    private int poolSoundId;
    private Context context;
    private boolean loaded = false;

    public SoundLoader(Context context, int soundRawId, SoundPool.OnLoadCompleteListener loadCompleteListener) {
        this.context = context;
        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        // Load the sound
        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        poolSoundId = loadSound(soundRawId);
        soundPool.setOnLoadCompleteListener(loadCompleteListener);

    }

    private int loadSound(int soundRawId) {
        return soundPool.load(context, soundRawId, 1);
    }

    public void execute() {
        soundPool.play(poolSoundId, 1, 1, 1, 0, 1.0f);
    }


    /**
     * @return the loaded
     */
    public boolean isLoaded() {
        return loaded;
    }

    /**
     * @param loaded the loaded to set
     */
    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }
}