package ar.com.imaknow.hiddenanimals.ui.game;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ar.com.imaknow.hiddenanimals.HiddenAnimalsApp;
import ar.com.imaknow.hiddenanimals.R;
import ar.com.imaknow.hiddenanimals.misc.ApplicationData;
import ar.com.imaknow.hiddenanimals.misc.Constants;
import ar.com.imaknow.hiddenanimals.misc.FragmentHandler;
import ar.com.imaknow.hiddenanimals.misc.TouchViewNoZoom;
import ar.com.imaknow.hiddenanimals.misc.Utils;
import ar.com.imaknow.hiddenanimals.models.MyImage;
import ar.com.imaknow.hiddenanimals.presenters.GamePresenter;
import ar.com.imaknow.hiddenanimals.ui.BaseActivity;
import ar.com.imaknow.hiddenanimals.ui.main.MainActivity;

/**
 * A placeholder fragment containing a simple view.
 */
public class GameFragment extends Fragment implements IGameView, View.OnTouchListener {
    public static final String TAG = "GameFragment";
    private static final int ONE_SECOND_IN_MILS = 1000;
    private static final int MIN_SECONDS_LIFE = 3;
    public static final int IMG_TRANSITION_DURATION_MILLIS = 300;

    private RelativeLayout blinkLayout;
    private TouchViewNoZoom touchView;
    private TextView txtVwCountDown;
    private TextView txtVwScore;
    private Drawable drawable;
    private MyImage myImage;

    private GamePresenter gamePresenter;
    private CountDownTimer countDownTimer;

    private ImageView life1, life2, life3;
    private int secondsRemaining = -1;
    private long milisecondsRemaining = 0;

    private Animation numberAnimation;
    private Animation lifeAnimation;

    private Resources.Theme theme;

    private TextView txtVwCoordinates;
    private TextView txtVwPorcentage;
    private boolean wasPaused;

    private TextView txtVwImageName;
    float initialX, initialY;
    boolean cutEvent = false;

    private Bundle bundle = new Bundle();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gamePresenter = new GamePresenter(getActivity(), this);
        gamePresenter.loadSounds();
        bundle = getArguments();

        // Show click head tip for level 1.
        if (bundle != null && bundle.getString(Constants.LEVEL_SELECTED).equals(Constants.LEVEL_1)) {
            Utils.showTapHeadTip(getActivity());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_game, container, false);
        theme = getActivity().getTheme();

        ((BaseActivity) getActivity()).initSoundButton(rootView);
        initializeLives(rootView);
        initializeAnimations();
        startDevelopmentConfig(rootView);

        blinkLayout = (RelativeLayout) rootView.findViewById(R.id.blink_layout);
        txtVwCountDown = (TextView) rootView.findViewById(R.id.txt_vw_countdown);
        txtVwScore = (TextView) rootView.findViewById(R.id.txt_vw_score);

        addTypefaces();

        touchView = (TouchViewNoZoom) rootView.findViewById(R.id.img_vw_picture);
        touchView.setOnTouchListener(this);
        showNextImage();
        startCountDown();

        return rootView;
    }

    private void startDevelopmentConfig(View rootView) {
        if (HiddenAnimalsApp.IS_DEVELOPMENT) {
            RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.rl_parameters_container);
            rl.setVisibility(View.VISIBLE);
            txtVwCoordinates = (TextView) rootView.findViewById(R.id.txt_vw_coordinates);
            txtVwPorcentage = (TextView) rootView.findViewById(R.id.txt_vw_porcentage);

            Button button = (Button) rootView.findViewById(R.id.btn_next);
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showNextImage();
                }
            });
            txtVwImageName = (TextView) rootView.findViewById(R.id.txt_vw_image_name);
            txtVwImageName.setVisibility(View.VISIBLE);
        }
    }

    private void initializeLives(View rootView) {
        life1 = (ImageView) rootView.findViewById(R.id.life1);
        life2 = (ImageView) rootView.findViewById(R.id.life2);
        life3 = (ImageView) rootView.findViewById(R.id.life3);
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                initialX = event.getX();
                initialY = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                float finalX = event.getX();
                float finalY = event.getY();
                if (!gamePresenter.isMoving(initialX, finalX, initialY, finalY) && !cutEvent) {
                    showEventParameters(event);
                    gamePresenter.evaluateTap(myImage, getTapedX(event), getTapedY(event));
                }
                resetCutEvent();
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                cutEvent = true;
                break;
        }

        return true;
    }

    private void showEventParameters(MotionEvent event) {
        if (HiddenAnimalsApp.IS_DEVELOPMENT) {
            txtVwCoordinates.setText("Touch coordinates : " + event.getRawX() + " - " + event.getRawY());
            txtVwPorcentage.setText("X: " + (event.getX() * 100 / getScreenWidth()) + " - Y: " +
                    (event.getY() * 100 / getScreenHeight()));
        }
    }

    @Override
    public void navigateToMenu() {
        if (getActivity() != null) {
            startActivity(new Intent(getActivity(), MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }
    }

    @Override
    public void startCountDown() {
        if (getActivity() != null) {
            secondsRemaining = myImage.getDifficulty().getTimeSeconds();
            milisecondsRemaining = secondsRemaining * ONE_SECOND_IN_MILS + ONE_SECOND_IN_MILS;
            programCountDown();
            countDownTimer.start();
        }
    }

    private void programCountDown() {
        countDownTimer = new CountDownTimer(milisecondsRemaining, ONE_SECOND_IN_MILS) {
            public void onTick(long millisUntilFinished) {
                milisecondsRemaining = millisUntilFinished;
                txtVwCountDown.setText(String.valueOf(millisUntilFinished / ONE_SECOND_IN_MILS - 1));
                animateNumber();
                reduceOneSecond();
                if (secondsRemaining == MIN_SECONDS_LIFE) {
                    gamePresenter.animateLife();
                }
            }

            public void onFinish() {
                clearNumberAnimation();
                clearLifeAnimation();
                gamePresenter.sendGALostLifeTimeEvent(getCurrentImage().getImageName());
                gamePresenter.removeLife();
                gamePresenter.playLoseLifeSound();
                gamePresenter.remarkRemovedLife();
                gamePresenter.checkLivesQty();
                gamePresenter.clearFastTapCounter();
                gamePresenter.clearTapCounter();
            }
        };
    }

    @Override
    public void animateNumber() {
        txtVwCountDown.startAnimation(numberAnimation);
    }

    @Override
    public void initializeAnimations() {
        numberAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.enlarge_countdown);
        lifeAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.reduce_life);
    }

    @Override
    public void addTypefaces() {
        Typeface tp = Typeface.createFromAsset(getActivity().getAssets(), ApplicationData.ANDROID_FONT);
        txtVwCountDown.setTypeface(tp);
        txtVwScore.setTypeface(tp);
    }

    @Override
    public void clearNumberAnimation() {
        txtVwCountDown.clearAnimation();
    }

    @Override
    public void clearLifeAnimation() {
        life1.clearAnimation();
        life2.clearAnimation();
        life3.clearAnimation();
    }

    @Override
    public void blinkScreen() {
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(getAlphaAnimation());
        animationSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                blinkLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                blinkLayout.setVisibility(View.GONE);
            }
        });

        blinkLayout.startAnimation(animationSet);
    }

    @Override
    public void openGameOverScreen() {
        bundle.putInt(ApplicationData.CURRENT_SCORE, getScore());
        FragmentHandler.getInstance().openGameOverFragment((AppCompatActivity) getActivity(), bundle);
    }

    @Override
    public void winGame() {
        gamePresenter.updateCompletedLevel(bundle);
        bundle.putInt(ApplicationData.CURRENT_SCORE, getScore());
        bundle.putBoolean(Constants.WIN_LEVEL, true);
        String levelSelected = bundle.getString(Constants.LEVEL_SELECTED);
        if (levelSelected != null && levelSelected.equals(Constants.LEVEL_GO_FOR_ALL)) {
            FragmentHandler.getInstance().openWinnerFragment((AppCompatActivity) getActivity(), bundle);
        } else {
            FragmentHandler.getInstance().openLevelCompletedFragment((AppCompatActivity) getActivity(), bundle);
        }
    }

    @Override
    public int getScore() {
        return Integer.valueOf(txtVwScore.getText().toString());
    }

    @Override
    public void pause() {
        stopCountDown();
        wasPaused = true;
    }

    @Override
    public void resume() {
        if (countDownTimer != null && wasPaused) {
            programCountDown();
            countDownTimer.start();
            wasPaused = false;
        }
    }

    private static AlphaAnimation getAlphaAnimation() {
        AlphaAnimation alphaAnim = new AlphaAnimation(1.0f, 0.0f);
        alphaAnim.setDuration(500);
        return alphaAnim;
    }


    @Override
    public void stopCountDown() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @Override
    public void removeLife1() {
        life1.setVisibility(View.INVISIBLE);
    }

    @Override
    public void removeLife2() {
        life2.setVisibility(View.INVISIBLE);
    }

    @Override
    public void removeLife3() {
        life3.setVisibility(View.INVISIBLE);
    }

    @Override
    public void animateLife1() {
        life1.startAnimation(lifeAnimation);
    }

    @Override
    public void animateLife2() {
        life2.startAnimation(lifeAnimation);
    }

    @Override
    public void animateLife3() {
        life3.startAnimation(lifeAnimation);
    }

    @Override
    public int getSecondsRemaining() {
        return secondsRemaining;
    }

    @Override
    public void reduceOneSecond() {
        secondsRemaining--;
    }

    @Override
    public MyImage getCurrentImage() {
        return myImage;
    }

    @Override
    public void updateScore(int points) {
        int currentPoints = Integer.valueOf(txtVwScore.getText().toString());
        txtVwScore.setText(String.valueOf(currentPoints + points));
    }

    @Override
    public void showNextImage() {
        myImage = gamePresenter.getNextImage();
        drawable = getImageDrawable(myImage);
        TransitionDrawable transition = getTransitionImage(drawable);
        touchView.setImageDrawable(transition);
        transition.startTransition(IMG_TRANSITION_DURATION_MILLIS);

        if (HiddenAnimalsApp.IS_DEVELOPMENT && myImage != null) {
            txtVwImageName.setText(myImage.getImageName());
        }
    }

//    @Override
//    public void showNextImage() {
//        if (gamePresenter.isLastImage(index)) {
//            winGame();
//            stopCountDown();
//        } else {
//            myImage = gamePresenter.getNextImage(index);
//            drawable = getImageDrawable(myImage);
//            TransitionDrawable transition = getTransitionImage(getImageDrawable(myImage));
//            touchView.setImageDrawable(transition);
//            transition.startTransition(IMG_TRANSITION_DURATION_MILLIS);
//            index++;
//        }
//
//        if (HiddenAnimalsApp.IS_DEVELOPMENT && myImage != null) {
//            txtVwImageName.setText(myImage.getImageName());
//        }
//    }

    private TransitionDrawable getTransitionImage(Drawable imageDrawable) {
        Drawable[] layers = new Drawable[2];
        layers[0] = getResources().getDrawable(R.drawable.game_background);
        layers[1] = imageDrawable;
        TransitionDrawable transition = new TransitionDrawable(layers);
        return transition;
    }

    @Override
    public Drawable getImageDrawable(MyImage myImage) {
        if (Build.VERSION.SDK_INT < 21) {
            return getResources().getDrawable(myImage.getResource());
        } else {
            return getResources().getDrawable(myImage.getResource(), theme);
        }
    }

    @Override
    public int getScreenWidth() {
        return touchView.getMeasuredWidth();
    }

    @Override
    public int getScreenHeight() {
        return touchView.getMeasuredHeight();
    }

    @Override
    public void resetZoom() {
        touchView.resetZoom();
    }

    @Override
    public void resetCutEvent() {
        cutEvent = false;
    }

    public float getTapedX(MotionEvent event) {
        RectF rect = touchView.getZoomedRect();
        float zoomedImageWidth = (rect.right - rect.left) * getScreenWidth();
        float porcentageTapedX = Utils.calculatePorcentage(event.getX(), getScreenWidth());
        float cornerXpoint = rect.left * getScreenWidth();
        float porcentageCornerX = Utils.calculatePorcentage(cornerXpoint, getScreenWidth());
        float relationalTapZoomedPorcentageX = zoomedImageWidth / getScreenWidth() * porcentageTapedX;
        return porcentageCornerX + relationalTapZoomedPorcentageX;
    }

    public float getTapedY(MotionEvent event) {
        RectF rect = touchView.getZoomedRect();
        float zoomedImageHeight = (rect.bottom - rect.top) * getScreenHeight();
        float porcentageTapedY = Utils.calculatePorcentage(event.getY(), getScreenHeight());
        float cornerYpoint = rect.top * getScreenHeight();
        float porcentageCornerY = Utils.calculatePorcentage(cornerYpoint, getScreenHeight());
        float relationalTapZoomedPorcentageY = zoomedImageHeight / getScreenHeight() * porcentageTapedY;
        return porcentageCornerY + relationalTapZoomedPorcentageY;
    }

    @Override
    public void onPause() {
        super.onPause();
        pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        resume();
    }
}
