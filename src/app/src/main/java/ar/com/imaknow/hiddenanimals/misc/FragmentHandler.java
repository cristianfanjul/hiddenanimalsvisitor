package ar.com.imaknow.hiddenanimals.misc;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import ar.com.imaknow.hiddenanimals.R;
import ar.com.imaknow.hiddenanimals.ui.game.GameFragment;
import ar.com.imaknow.hiddenanimals.ui.gameover.GameOverFragment;
import ar.com.imaknow.hiddenanimals.ui.winner.LevelCompletedFragment;
import ar.com.imaknow.hiddenanimals.ui.winner.WinnerFragment;

public class FragmentHandler {

    private static FragmentHandler instance = new FragmentHandler();

    public static FragmentHandler getInstance() {
        return instance;
    }

    public void openGameFragment(AppCompatActivity activity, Bundle bundle) {
        clearFragmentStack(activity);
        replaceFragment(activity, new GameFragment(), bundle);
    }

    public void openGameOverFragment(AppCompatActivity activity, Bundle bundle) {
        clearFragmentStack(activity);
        replaceFragment(activity, new GameOverFragment(), bundle);
    }

    public void openLevelCompletedFragment(AppCompatActivity activity, Bundle bundle) {
        clearFragmentStack(activity);
        replaceFragment(activity, new LevelCompletedFragment(), bundle);
    }

    public void openWinnerFragment(AppCompatActivity activity, Bundle bundle) {
        clearFragmentStack(activity);
        replaceFragment(activity, new WinnerFragment(), bundle);
    }

    private void replaceFragment(AppCompatActivity activity, Fragment fragment, Bundle bundle) {
        String backStateName = fragment.getClass().getSimpleName();
        String fragmentTag = backStateName;

        FragmentManager manager = activity.getSupportFragmentManager();

        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.fragment, fragment, fragmentTag);
        fragment.setArguments(bundle);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    public void clearFragmentStack(AppCompatActivity activity) {
        activity.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
}