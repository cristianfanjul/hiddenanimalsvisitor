package ar.com.imaknow.hiddenanimals.ui.gameover;

import ar.com.imaknow.hiddenanimals.ui.game.IEndGamePresenter;

/**
 * Created by Acer VN7 on 11/08/2015.
 */
public interface IGameOverPresenter extends IEndGamePresenter {
}
