package ar.com.imaknow.hiddenanimals.presenters;

import android.content.Context;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import ar.com.imaknow.hiddenanimals.HiddenAnimalsApp;
import ar.com.imaknow.hiddenanimals.R;
import ar.com.imaknow.hiddenanimals.misc.ApplicationData;
import ar.com.imaknow.hiddenanimals.misc.Constants;
import ar.com.imaknow.hiddenanimals.misc.SoundLoader;
import ar.com.imaknow.hiddenanimals.models.ImageList;
import ar.com.imaknow.hiddenanimals.models.MyImage;
import ar.com.imaknow.hiddenanimals.ui.game.IGameView;

/**
 * Created by Acer VN7 on 15/07/2015.
 */
public class GamePresenter implements IGamePresenter {
    private static final float MOVE_TRESHOLD = 50f;
    public static final int LIFE_TAPS_LIMIT = 10;
    public static final int FAST_TAPS_LIMIT = 5;
    private static final long FAST_TAP_TIME = 3 * 1000;
    public static final int LIFE_INDEX = 3;
    private IGameView view;
    private int lifeIndex;
    private int wrongTapCounter;
    private int wrongFastTapCounter;
    private Context context;
    private SoundLoader correctSoundLoader;
    private SoundLoader loseLifeSoundLoader;
    private ApplicationData appData;

    public GamePresenter(Context context, IGameView gameView) {
        this.context = context;
        this.view = gameView;
        this.lifeIndex = LIFE_INDEX;
        clearTapCounter();
        clearFastTapCounter();
        appData = new ApplicationData(context);
    }

    @Override
    public void startGame() {

    }

    @Override
    public MyImage getNextImage() {
        return ImageList.getInstance().getNextImage();
    }

    @Override
    public MyImage getNextImage(int currentIndex) {
        return ImageList.getInstance().getNextImage(currentIndex);
    }

    @Override
    public boolean isLastImage() {
        return ImageList.getInstance().isLastImage();
    }

    @Override
    public boolean isLastImage(int currentIndex) {
        return ImageList.getInstance().isLastImage(currentIndex);
    }

    @Override
    public boolean tapCorrectPoint(MyImage myImage, float tapedX, float tapedY) {
        return myImage.isCorrectRange(tapedX, tapedY);
    }

    @Override
    public void sumPoints() {
        view.updateScore(view.getCurrentImage().getDifficulty().getPoints() * view.getSecondsRemaining());
    }

    @Override
    public void remarkRemovedLife() {
        view.blinkScreen();
    }

    @Override
    public void removeLife() {
        switch (lifeIndex) {
            case 1:
                view.removeLife1();
            case 2:
                view.removeLife2();
            case 3:
                view.removeLife3();
            default:
                break;
        }
        lifeIndex--;
    }

    @Override
    public void checkLivesQty() {
        if (lifeIndex > 0) {
            view.startCountDown();
        } else {
            endGame();
        }
    }

    @Override
    public void endGame() {
        view.stopCountDown();
        view.openGameOverScreen();
    }

    @Override
    public void animateLife() {
        switch (lifeIndex) {
            case 1:
                view.animateLife1();
                break;
            case 2:
                view.animateLife2();
                break;
            case 3:
                view.animateLife3();
                break;
        }
    }

    @Override
    public void addWrongTap() {
        wrongTapCounter++;
    }

    @Override
    public void addWrongFastTap() {
        this.wrongFastTapCounter++;
    }

    @Override
    public void clearTapCounter() {
        this.wrongTapCounter = 0;
    }

    @Override
    public void clearFastTapCounter() {
        this.wrongFastTapCounter = 0;
    }

    @Override
    public void checkMultipleWrongTaps() {
        if (isFastTapLimit() || isWrongTapLimit()) {
            view.stopCountDown();
            view.clearNumberAnimation();
            view.clearLifeAnimation();
            sendGALostLifeTouchesEvent(view.getCurrentImage().getImageName());
            removeLife();
            playLoseLifeSound();
            remarkRemovedLife();
            checkLivesQty();
            clearTapCounter();
            clearFastTapCounter();
        }
    }

    @Override
    public void fireFastTapsHandler() {
        if (this.wrongFastTapCounter == 0) {
            Handler handler = new Handler();
            Runnable runnable = new Runnable() {
                public void run() {
                    clearFastTapCounter();
                }
            };
            handler.postDelayed(runnable, FAST_TAP_TIME);
        }
        addWrongFastTap();
    }

    @Override
    public boolean isWrongTapLimit() {
        if (this.wrongTapCounter == LIFE_TAPS_LIMIT) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean isFastTapLimit() {
        if (this.wrongFastTapCounter == FAST_TAPS_LIMIT) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void loadSounds() {
        correctSoundLoader = new SoundLoader(this.context, R.raw.correct, new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                correctSoundLoader.setLoaded(true);
            }
        });
        loseLifeSoundLoader = new SoundLoader(this.context, R.raw.wrong, new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                loseLifeSoundLoader.setLoaded(true);
            }
        });
    }

    @Override
    public void playCorrectSound() {
        if (correctSoundLoader.isLoaded() && appData.isSoundEnabled()) {
            correctSoundLoader.execute();
        }
    }

    @Override
    public void playLoseLifeSound() {
        if (loseLifeSoundLoader.isLoaded() && appData.isSoundEnabled()) {
            loseLifeSoundLoader.execute();
        }
    }

    public void evaluateTap(MyImage myImage, float tapedX, float tapedY) {
        if (tapCorrectPoint(myImage, tapedX, tapedY)) {
            playCorrectSound();
            sumPoints();
            view.stopCountDown();
            view.resetZoom();
            view.clearLifeAnimation();
            clearTapCounter();
            clearFastTapCounter();
            if (isLastImage()) {
                view.stopCountDown();
                view.winGame();
            } else {
                view.showNextImage();
                view.resetCutEvent();
                view.startCountDown();
            }
        } else {
            addWrongTap();
            fireFastTapsHandler();
            checkMultipleWrongTaps();
        }
    }

    @Override
    public boolean isMoving(float initialX, float finalX, float initialY, float finalY) {
        float differenceX = Math.abs(finalX) - Math.abs(initialX);
        float differenceY = Math.abs(finalY) - Math.abs(initialY);
        if (differenceX >= MOVE_TRESHOLD || differenceY >= MOVE_TRESHOLD) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void updateCompletedLevel(Bundle bundle) {
        String completedLevel = bundle.getString(Constants.LEVEL_SELECTED);
        if (completedLevel.equals(Constants.LEVEL_1)) {
            appData.setLevel1Completed(true);
        } else if (completedLevel.equals(Constants.LEVEL_2)) {
            appData.setLevel2Completed(true);
        } else if (completedLevel.equals(Constants.LEVEL_3)) {
            appData.setLevel3Completed(true);
        }
    }

    @Override
    public void sendGALostLifeTimeEvent(String imageName) {
        Tracker t = HiddenAnimalsApp.getTracker();
        t.send(new HitBuilders.EventBuilder()
                .setCategory(context.getString(R.string.ga_category_lost_life))
                .setAction(context.getString(R.string.ga_event_lost_life_time))
                .setLabel(imageName)
                .build());
    }

    @Override
    public void sendGALostLifeTouchesEvent(String imageName) {
        Tracker t = HiddenAnimalsApp.getTracker();
        t.send(new HitBuilders.EventBuilder()
                .setCategory(context.getString(R.string.ga_category_lost_life))
                .setAction(context.getString(R.string.ga_event_lost_life_touches))
                .setLabel(imageName)
                .build());
    }
}
