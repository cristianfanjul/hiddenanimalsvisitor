package ar.com.imaknow.hiddenanimals.ui.game;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.View;

import ar.com.imaknow.hiddenanimals.HiddenAnimalsApp;
import ar.com.imaknow.hiddenanimals.R;
import ar.com.imaknow.hiddenanimals.misc.Constants;
import ar.com.imaknow.hiddenanimals.misc.FragmentHandler;
import ar.com.imaknow.hiddenanimals.misc.MyCustomDialog;
import ar.com.imaknow.hiddenanimals.models.ImageList;
import ar.com.imaknow.hiddenanimals.ui.BaseActivity;

public class GameActivity extends BaseActivity {

    private static final long FINISH_DELAY_TIME = 100;
    private MyCustomDialog dialog;
    private Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ImageList.getInstance().clear();
        createLevelImageList(getIntent().getExtras());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        FragmentHandler.getInstance().openGameFragment(this, getIntent().getExtras());
        dialog = new MyCustomDialog(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        fragment = getSupportFragmentManager().findFragmentById(R.id.fragment);
        dialog.dismiss();
    }

    private void createLevelImageList(Bundle extras) {
        if (extras != null) {
            String level = extras.getString(Constants.LEVEL_SELECTED);
            if (level != null) {
                if (level.equals(Constants.LEVEL_1)) {
                    ImageList.getInstance().createLevelOneImageList();
                    HiddenAnimalsApp.getTracker().setScreenName(getString(R.string.ga_level_1));
                } else if (level.equals(Constants.LEVEL_2)) {
                    ImageList.getInstance().createLevelTwoImageList();
                    HiddenAnimalsApp.getTracker().setScreenName(getString(R.string.ga_level_2));
                } else if (level.equals(Constants.LEVEL_3)) {
                    ImageList.getInstance().createLevelThreeImageList();
                    HiddenAnimalsApp.getTracker().setScreenName(getString(R.string.ga_level_3));
                } else if (level.equals(Constants.LEVEL_GO_FOR_ALL)) {
                    ImageList.getInstance().createFullImageList();
                    HiddenAnimalsApp.getTracker().setScreenName(getString(R.string.ga_level_go_for_all));
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (fragment != null && fragment.getTag().equals(GameFragment.TAG)) {
            fragment.onPause();
            dialog.setTitle(getString(R.string.quit_game));
            dialog.setMessage(getString(R.string.quit_game_details));
            dialog.setPositiveButtonListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    fireFinishActivity();
                }
            });
            dialog.setNegativeButtonListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    fragment.onResume();
                }
            });
            dialog.show();
        } else {
            finish();
        }
    }

    private void fireFinishActivity() {
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            public void run() {
                finish();
            }
        };
        handler.postDelayed(runnable, FINISH_DELAY_TIME);
    }
}
