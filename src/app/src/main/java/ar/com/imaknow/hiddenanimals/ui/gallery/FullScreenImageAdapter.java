package ar.com.imaknow.hiddenanimals.ui.gallery;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.List;

import ar.com.imaknow.hiddenanimals.R;
import ar.com.imaknow.hiddenanimals.misc.TouchImageView;
import ar.com.imaknow.hiddenanimals.models.MyImage;

public class FullScreenImageAdapter extends PagerAdapter {

    private Activity mActivity;
    private List<MyImage> mMyImageList;
    private LayoutInflater inflater;

    // constructor
    public FullScreenImageAdapter(Activity activity, List<MyImage> myImageList) {
        mActivity = activity;
        mMyImageList = myImageList;
    }

    @Override
    public int getCount() {
        return mMyImageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TouchImageView imgDisplay;

        inflater = (LayoutInflater) mActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container, false);

        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);

        MyImage img = mMyImageList.get(position);

        imgDisplay.setImageResource(img.getResource());

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
    }

}
