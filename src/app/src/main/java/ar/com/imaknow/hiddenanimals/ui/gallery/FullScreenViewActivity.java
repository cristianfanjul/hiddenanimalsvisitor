package ar.com.imaknow.hiddenanimals.ui.gallery;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;

import ar.com.imaknow.hiddenanimals.HiddenAnimalsApp;
import ar.com.imaknow.hiddenanimals.R;
import ar.com.imaknow.hiddenanimals.misc.ApplicationData;
import ar.com.imaknow.hiddenanimals.models.ImageList;
import ar.com.imaknow.hiddenanimals.ui.BaseActivity;

public class FullScreenViewActivity extends BaseActivity {
    private static final long DELAY_TIME_INTERSTITIAL = 1000;

    private FullScreenImageAdapter adapter;
    private ViewPager viewPager;
    private boolean isPaused = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        initializeImageList();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_view);
        HiddenAnimalsApp.getTracker().setScreenName(getString(R.string.ga_full_screen_image));

        viewPager = (ViewPager) findViewById(R.id.pager);

        Intent i = getIntent();
        int position = i.getIntExtra("position", 0);

        adapter = new FullScreenImageAdapter(FullScreenViewActivity.this,
                ImageList.getInstance().getMyImageList());

        viewPager.setAdapter(adapter);

        // displaying selected image first
        viewPager.setCurrentItem(position);

        showInterstitialWithDelay();
    }

    public void showInterstitialWithDelay() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isPaused) {
                    showInterstitial();
                }
            }
        }, DELAY_TIME_INTERSTITIAL);
    }

    @Override
    public void onResume() {
        super.onResume();
        isPaused = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        isPaused = true;
    }

    private void initializeImageList() {
        if (ImageList.getInstance().getMyImageList().size() == 0) {
            ApplicationData appData = new ApplicationData(this);
            if (appData.isLevel1Completed()) {
                ImageList.getInstance().createLevelOneImageList();
            }

            if (appData.isLevel2Completed()) {
                ImageList.getInstance().createLevelOneImageList();
            }

            if (appData.isLevel3Completed()) {
                ImageList.getInstance().createLevelOneImageList();
            }
        }
    }
}
