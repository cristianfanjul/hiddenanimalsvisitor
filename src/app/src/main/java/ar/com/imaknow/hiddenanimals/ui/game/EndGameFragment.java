package ar.com.imaknow.hiddenanimals.ui.game;

import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import ar.com.imaknow.hiddenanimals.HiddenAnimalsApp;
import ar.com.imaknow.hiddenanimals.R;
import ar.com.imaknow.hiddenanimals.misc.ApplicationData;
import ar.com.imaknow.hiddenanimals.misc.Constants;
import ar.com.imaknow.hiddenanimals.misc.GameButton;
import ar.com.imaknow.hiddenanimals.ui.BaseActivity;

/**
 * Created by Acer VN7 on 01/08/2015.
 */
public abstract class EndGameFragment extends Fragment implements IEndGameView, View.OnClickListener {

    private static final long DELAY_TIME_INTERSTITIAL = 1000;
    private TextView txtVwScoreTitle;
    private TextView txtVwScoreSubtitle;
    private TextView txtVwScoreValue;
    private GameButton btnNextOrRestart;
    private GameButton btnMainMenu;
    private EndGamePresenter endGamePresenter;
    private boolean isPaused;
    protected Bundle bundle;
    protected boolean isClickEnabled;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        endGamePresenter = new EndGamePresenter(this, (GameActivity) getActivity());
        endGamePresenter.evaluateMaxScore(getArguments());
        bundle = getArguments();
    }

    private void enableClicks() {
        isClickEnabled = true;
    }

    private void disableClicks() {
        isClickEnabled = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        isPaused = false;
        enableClicks();
    }

    @Override
    public void onPause() {
        super.onPause();
        isPaused = true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutForFragment(), container, false);

        initializeViews(rootView);

        if (!HiddenAnimalsApp.IS_DEVELOPMENT) {
            showInterstitialWithDelay();
        }

        setScore(bundle);

        addTypefaces();

        loadAdBanner(rootView);

        return rootView;
    }

    public void showInterstitialWithDelay() {
        final BaseActivity activity = (BaseActivity) getActivity();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isPaused) {
                    activity.showInterstitial();
                }
            }
        }, DELAY_TIME_INTERSTITIAL);
    }

    protected abstract int getLayoutForFragment();

    private void initializeViews(View rootView) {
        txtVwScoreTitle = (TextView) rootView.findViewById(R.id.txt_vw_game_over_title);
        txtVwScoreSubtitle = (TextView) rootView.findViewById(R.id.txt_vw_end_game_score_subtitle);
        txtVwScoreValue = (TextView) rootView.findViewById(R.id.txt_vw_end_game_score);
        btnNextOrRestart = (GameButton) rootView.findViewById(R.id.btn_restart);
        btnNextOrRestart.setOnClickListener(this);
        if (isNextLevelAvailable()) {
            btnNextOrRestart.setText(getString(R.string.next_level));
        }
        btnMainMenu = (GameButton) rootView.findViewById(R.id.btn_main_menu);
        btnMainMenu.setOnClickListener(this);

        initFacebookButton(rootView);
        initTwitterButton(rootView);
    }

    protected void initTwitterButton(View rootView) {
        Button btnTwitter = (Button) rootView.findViewById(R.id.btn_twitter);
        btnTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TweetComposer.Builder builder = new TweetComposer.Builder(getActivity())
                        .text(getSharingMessage().toString())
                        .image(Uri.parse(getString(R.string.url)));
                builder.show();
            }
        });
    }

    protected StringBuilder getSharingMessage() {
        StringBuilder stringBuilder = new StringBuilder(getString(R.string.social_share_content) + " " + getScore());
        stringBuilder.append('\n');
        stringBuilder.append(getString(R.string.url));
        return stringBuilder;
    }

    protected void initFacebookButton(View rootView) {
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentTitle(getString(R.string.facebook_share_title))
                .setContentDescription(getString(R.string.social_share_content) + " " + getScore())
                .setContentUrl(Uri.parse(getString(R.string.url)))
                .build();

        ShareButton shareButton = (ShareButton) rootView.findViewById(R.id.btn_share_fb);
        shareButton.setShareContent(content);
    }

    @Override
    public void addTypefaces() {
        Typeface tp = Typeface.createFromAsset(getActivity().getAssets(), ApplicationData.ANDROID_FONT);
        txtVwScoreTitle.setTypeface(tp);
        txtVwScoreSubtitle.setTypeface(tp);
        txtVwScoreValue.setTypeface(tp);
        btnNextOrRestart.setTypeface(tp);
        btnMainMenu.setTypeface(tp);
    }

    @Override
    public void onClick(View v) {
        if (isClickEnabled) {
            disableClicks();
            switch (v.getId()) {
                case R.id.btn_restart:
                    if (isNextLevelAvailable()) {
                        endGamePresenter.startNextLevel(getCurrentLevel(), bundle);
                    } else {
                        endGamePresenter.restartGame(getCurrentLevel());
                    }
                    break;
                case R.id.btn_main_menu:
                    endGamePresenter.openMainMenuScreen();
                    break;
            }
        }
    }

    @Override
    public void setScore(Bundle bundle) {
        if (bundle != null) {
            int score = bundle.getInt(ApplicationData.CURRENT_SCORE);
            txtVwScoreValue.setText(String.valueOf(score));
        }
    }

    @Override
    public int getScore() {
        if (bundle != null) {
            return bundle.getInt(ApplicationData.CURRENT_SCORE);
        } else {
            return 0;
        }
    }

    /**
     * Checks is the current level has been won and there is a next one available.
     *
     * @return
     */
    private boolean isNextLevelAvailable() {
        String levelSelected = getCurrentLevel();
        Boolean winLevel = bundle.getBoolean(Constants.WIN_LEVEL, false);
        if (levelSelected == null || levelSelected.equals(Constants.LEVEL_GO_FOR_ALL) || !winLevel) {
            return false;
        }
        return true;
    }

    public String getCurrentLevel() {
        return bundle.getString(Constants.LEVEL_SELECTED);
    }

    private void loadAdBanner(View rootView) {
        // Load an ad into the AdMob banner view.
        AdView adView = (AdView) rootView.findViewById(R.id.ad_view);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }
}
