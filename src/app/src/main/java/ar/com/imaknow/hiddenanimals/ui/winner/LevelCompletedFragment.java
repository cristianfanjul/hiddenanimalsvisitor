package ar.com.imaknow.hiddenanimals.ui.winner;

import android.os.Bundle;
import android.support.annotation.Nullable;

import ar.com.imaknow.hiddenanimals.HiddenAnimalsApp;
import ar.com.imaknow.hiddenanimals.R;
import ar.com.imaknow.hiddenanimals.ui.game.EndGameFragment;

/**
 * Created by Acer VN7 on 01/08/2015.
 */
public class LevelCompletedFragment extends EndGameFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HiddenAnimalsApp.getTracker().setScreenName(getString(R.string.ga_win_level));
    }

    @Override
    protected int getLayoutForFragment() {
        return R.layout.fragment_level_completed;
    }
}
