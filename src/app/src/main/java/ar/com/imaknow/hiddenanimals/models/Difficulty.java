package ar.com.imaknow.hiddenanimals.models;

/**
 * Created by Acer VN7 on 13/07/2015.
 */
public class Difficulty {
    private DifficultType type;
    private int timeSeconds;
    private int points;

    public Difficulty(DifficultType difficultType, int timeSeconds, int points) {
        this.type = difficultType;
        this.timeSeconds = timeSeconds;
        this.points = points;
    }

    public final DifficultType getType() {
        return type;
    }

    public final void setType(DifficultType type) {
        this.type = type;
    }

    public final int getTimeSeconds() {
        return timeSeconds;
    }

    public final void setTimeSeconds(int timeSeconds) {
        this.timeSeconds = timeSeconds;
    }

    public final int getPoints() {
        return points;
    }

    public final void setPoints(int points) {
        this.points = points;
    }

}
