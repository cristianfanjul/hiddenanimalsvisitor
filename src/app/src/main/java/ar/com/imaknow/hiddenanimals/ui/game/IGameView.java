package ar.com.imaknow.hiddenanimals.ui.game;

import android.graphics.drawable.Drawable;

import ar.com.imaknow.hiddenanimals.models.MyImage;

/**
 * Created by Acer VN7 on 15/07/2015.
 */
public interface IGameView extends IBaseView {

    void navigateToMenu();

    /**
     * Loads the next image.
     */
    void showNextImage();

    /**
     * Get the drawable of an MyImage type;
     *
     * @param myImage
     * @return
     */
    Drawable getImageDrawable(MyImage myImage);

    /**
     * Obtains the screen width;
     *
     * @return
     */
    int getScreenWidth();

    /**
     * Obtains the screen height;
     *
     * @return
     */
    int getScreenHeight();

    /**
     * Removes the zoom aplied over an image;
     */
    void resetZoom();

    /**
     * Unblocks the key events.
     */
    void resetCutEvent();

    /**
     * Starts the countdown timer.
     */
    void startCountDown();

    /**
     * Stops the countdown timer.
     */
    void stopCountDown();

    void removeLife1();

    void removeLife2();

    void removeLife3();

    void animateLife1();

    void animateLife2();

    void animateLife3();

    /**
     * Obtains seconds remaining in the countdown.
     *
     * @return
     */
    int getSecondsRemaining();

    /**
     * Substracts one second to a seconds counter.
     */
    void reduceOneSecond();

    /**
     * Gets the type MyImage currently displayed in screen.
     *
     * @return
     */
    MyImage getCurrentImage();

    /**
     * The score is updated.
     *
     * @param points
     */
    void updateScore(int points);

    /**
     * Starts an animation over the countdown number.
     */
    void animateNumber();

    /**
     * Instantiates images.
     */
    void initializeAnimations();

    /**
     * Removes the animation from the countdown.
     */
    void clearNumberAnimation();

    /**
     * Removes the animation from the lives images.
     */
    void clearLifeAnimation();

    /**
     * Blinks the screen. This occurs when a life is lost.
     */
    void blinkScreen();

    /**
     * Replaces the game fragment for the game over fragment.
     */
    void openGameOverScreen();

    /**
     * Ends the game and shows the winner screen.
     */
    void winGame();

    /**
     * Obtains the current score.
     *
     * @return
     */
    int getScore();

    void pause();

    void resume();
}
