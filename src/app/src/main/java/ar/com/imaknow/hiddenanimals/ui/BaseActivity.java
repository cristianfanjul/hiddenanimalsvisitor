package ar.com.imaknow.hiddenanimals.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import ar.com.imaknow.hiddenanimals.HiddenAnimalsApp;
import ar.com.imaknow.hiddenanimals.R;
import ar.com.imaknow.hiddenanimals.misc.ApplicationData;

/**
 * Base {@link android.app.Activity} class for every activity in this application.
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mInterstitialAd == null && !HiddenAnimalsApp.IS_DEVELOPMENT) {
            // Create the InterstitialAd and set the adUnitId (defined in values/strings.xml).
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                }

                @Override
                public void onAdClosed() {
                }
            });
            loadInterstitial();
        }
    }


    public void showInterstitial() {
        // Show the ad if it's ready. Otherwise toast and reload the ad.
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            goToNextLevel();
        }
    }

    private void loadInterstitial() {
        if (mInterstitialAd != null) {
            AdRequest adRequest = new AdRequest.Builder().addTestDevice("3657EA52DCFEA50DAF3D3C54C68BEE70").build();
            mInterstitialAd.loadAd(adRequest);
        }
    }

    private void goToNextLevel() {
        loadInterstitial();
    }

    public void initSoundButton(View fragmentView) {
        final ImageView imgVwSound = (ImageView) fragmentView.findViewById(R.id.img_vw_sound);
        final ApplicationData appData = new ApplicationData(this);
        setSoundImageIcon(appData.isSoundEnabled(), imgVwSound);
        imgVwSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (appData.isSoundEnabled()) {
                    appData.setSoundEnabled(false);
                } else {
                    appData.setSoundEnabled(true);
                }
                setSoundImageIcon(appData.isSoundEnabled(), imgVwSound);
            }
        });

    }

    private void setSoundImageIcon(boolean value, ImageView imgVwSound) {
        if (value) {
            imgVwSound.setImageDrawable(getResources().getDrawable(R.drawable.sound));
        } else {
            imgVwSound.setImageDrawable(getResources().getDrawable(R.drawable.no_sound));
        }
    }

}